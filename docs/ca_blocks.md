You can configure the running of [Component Accumulator][ca] based code using "blocks".
Each block is a Python class that holds configuration keys as constructor arguments, and implements a `to_ca()` function that returns a `ComponentAccumulator` object.
Blocks are defined in the [`FTagDumper/python/blocks/`]({{repo_url}}-/blob/main/FTagDumper/python/blocks/) directory, and inherit from the [`BaseBlock`]({{repo_url}}-/blob/main/FTagDumper/python/blocks/BaseBlock.py) class.

Blocks are configured inside an optional `"ca_blocks": []` list in the top-level JSON configuration.
Each block is a dict with a `"block"` key that specifies the block name, and any other keys that are passed to the block constructor.

[ca]: https://atlassoftwaredocs.web.cern.ch/guides/ca_configuration/ca/

This page contains automatically generated documentation for the different 
CA blocks that can be configured with the dumpster.
