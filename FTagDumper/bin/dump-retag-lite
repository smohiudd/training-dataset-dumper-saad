#!/usr/bin/env python

"""Lightweight retagging script

Runs some variations and saves the GN2 scores on jets.

This is distinct from the "full" tagging that we use in most flavor
tagging. You can run that with `dump-retag`.

The main differences here are:

- Athena is optional (required for the "rich"  track extrapolation)
- No b-tagging object is created
- No secondary vertexing code is run, this is strictly NNs

"""

from argparse import ArgumentParser
import sys

from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.MainServicesConfig import MainServicesCfg as getConfig
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg

from FTagDumper import dumper
from FTagDumper import retag_lite as retag

import copy

def get_args():
    """
    Extend the base dumper argument parser.
    """
    parser = ArgumentParser(
        formatter_class=dumper.DumperHelpFormatter,
        parents=[dumper.base_parser(__doc__, add_help=False)],
    )
    return parser.parse_args()


def run():

    args = get_args()

    flags = dumper.update_flags(args)
    flags.lock()

    ca = getConfig(flags)
    ca.merge(PoolReadCfg(flags))
    ca.addService(CompFactory.AthenaEventLoopMgr(
        EventPrintoutInterval=1))

    config_all = dumper.combinedConfig(args.config_file)
    constargs = dict(
        output=args.output,
        force_full_precision=args.force_full_precision
    )

    # add one set of algs for each track and jet systematic
    all_syst_sets = set(['track_systematics', 'jet_systematics'])
    for syst_set in all_syst_sets:
        for syst in config_all[syst_set]:
            config = copy.deepcopy(config_all)
            config[syst_set] = [syst]
            for rmsys in all_syst_sets - set([syst_set]):
                config[rmsys] = []

            ca.merge(
                retag.dumpster_cfg(
                    flags,
                    config=config,
                    **constargs
                )
            )
    # now add nominal as well
    nom_config = copy.deepcopy(config_all)
    for rmsys in all_syst_sets:
        nom_config[rmsys] = []
    ca.merge(
        retag.dumpster_cfg(
            flags,
            config=nom_config,
            **constargs
        )
    )
    return ca.run()


if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
