#!/usr/bin/env bash

set -Eeu

print-usage() {
    echo "usage: ${0##*/} [-h]" 1>&2
}

usage() {
    print-usage
    exit 1;
}

BASE_DIR=$(dirname $(readlink -e ${BASH_SOURCE[0]}))/../..
CFG_DIR=${BASE_DIR}/configs

help() {
    print-usage
    cat <<EOF

Loop over all json files found in a directory, test if they parse as
single b-tagging configuration.

Options:
 -d: specify directory to run in
 -h: print help
 -v: verbose

If no -d directory is given use:

${CFG_DIR}

EOF
    exit 1
}

OUT=/dev/null

while getopts ":d:hv" o; do
    case "${o}" in
        d) CFG_DIR=${OPTARG} ;;
        v) OUT=/dev/stdout ;;
        h) help ;;
        *) usage ;;
    esac
done
shift $((OPTIND-1))

if (( $# != 0 )) ; then
    usage
fi

# fun with colors
RED=''
GREEN=''
UNSET=''
if [[ -t 1 ]]
then
    RED="\033[1;31m"
    GREEN="\033[32m"
    UNSET="\033[0m"
fi

# run the tests
RET_CODE=0
for FILE in ${CFG_DIR}/*.json
do
    BAD=""
    if ! test-config-parse-single-b $FILE &> $OUT
    then
        CFG="${RED}Error in Config${UNSET}"
        RET_CODE=1
        BAD=1
    else
        CFG="${GREEN}OK Config${UNSET}"
    fi

    if fgrep -q $'\t' $FILE &> $OUT
    then
        TABS="${RED}Found Tabs${UNSET}"
        RET_CODE=1
        BAD=1
    else
        TABS="${GREEN}No Tabs${UNSET}"
    fi
    if ! [[ ${BAD} ]]
    then
        echo -e "${FILE##*/}: ${CFG}, ${TABS}" > $OUT
    else
        echo -e "${FILE##*/}: ${CFG}, ${TABS}!" 1>&2
    fi
done
exit $RET_CODE
